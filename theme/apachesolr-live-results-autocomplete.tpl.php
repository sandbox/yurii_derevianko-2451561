<?php

/**
 * @file
 * Template to display autocomplete suggestions.
 */

?>

<div class="apachesolr-live-results suggestion">
  <strong><?php print $suggestion['same_part']; ?></strong>
  <?php print $suggestion['different_part']; ?>
</div>

<?php if ($count) : ?>
  <div class='apachesolr-live-results-count message'>
    <?php print t('!count results', array('!count' => $count)); ?>
  </div>
<?php endif; ?>

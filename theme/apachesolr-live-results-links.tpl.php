<?php

/**
 * @file
 * Template to display a live results suggestions.
 */

?>

<?php if (!isset($info)) : ?>
  <div class="apachesolr-live-results-header">
    <strong><?php print 'Potential results'; ?></strong>
  </div>
<?php else: ?>
  <div class="apachesolr-live-results suggestion apachesolr-content-link">
    <?php if (isset($info['label'])) : ?>
      <div class="apachesolr-live-results-label">
        <?php print $info['label']; ?>
      </div>
    <?php endif; ?>
    <?php foreach ($info['preproc'] as $value) : ?>
      <div class="<?php print $value['class']; ?>">
        <?php print $value['value']; ?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

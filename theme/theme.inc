<?php

/**
 * @file
 * Preprocessors and theme functions.
 */

/**
 * Implements hook_preprocess().
 *
 * For theme apachesolr_live_results_links.
 */
function apachesolr_live_results_preprocess_apachesolr_live_results_links(&$variables) {
  foreach ($variables['info'] as $delta => $value) {
    if ($delta !== 'link' && $delta !== 'label' && $value !== NULL) {

      if (stripos($delta, 'im') === 0) {
        $terms = NULL;
      
        if (is_array($value)) {
          foreach ($value as $index => $term) {
            $term_value = taxonomy_term_load($term);
            $term_name = $term_value->name;
            if ($index === 0) {
              $terms .= $term_name;
            }
            else {
              $terms .= ', ' . $term_name;
            }
          }
        }
        else {
          $term_value = taxonomy_term_load($value);
          $terms = $term_value->name;
        }

        $variables['info']['preproc'][$delta] = array(
          'value' => $terms,
          'class' => 'apachesolr-live-results-terms',
        );
        continue;
      }
      elseif (stripos($delta, 'bm') === 0) {
        $variables['info']['preproc'][$delta] = array(
          'value' => $value[0],
          'class' => 'apachesolr-live-results-bm',
        );
        continue;
      }
      elseif (drupal_valid_path($value)) {
        $arbitrary_array = array(
          'path' => $value,
          'width' => '75px',
          'height' => '75px',
        );
        $variables['info']['preproc'][$delta] = array(
          'value' => theme_image($arbitrary_array),
          'class' => 'apachesolr-live-results-img',
        );
        continue;
      }
      else {
        $variables['info']['preproc'][$delta] = array(
          'value' => $value,
          'class' => 'apachesolr-live-results-values',
        );
      }

    }
  }

}

/**
 * Implements hook_preprocess().
 *
 * For theme apachesolr_live_results_autocomplete.
 */
function apachesolr_live_results_preprocess_apachesolr_live_results_autocomplete(&$variables) {
  $variables['suggestion']['same_part'] = drupal_substr($variables['suggestion']['suggestion'], 0, strlen($variables['keys']));
  $variables['suggestion']['different_part'] = drupal_substr($variables['suggestion']['suggestion'], strlen($variables['keys']));
}

/**
 * Themes the spellchecker's suggestion.
 */
function theme_apachesolr_live_results_spellcheck($variables) {
  $suggestion_string = $variables['suggestion']['suggestion'];
  return '<span class="apachesolr_autocomplete message">' . t('Did you mean') . ':</span> ' . $suggestion_string;
}

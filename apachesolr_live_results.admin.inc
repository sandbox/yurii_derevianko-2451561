<?php
/**
 * @file
 * Provides form with AJAX REGISTER module settings.
 */

/**
 * Administrative settings form.
 */
function live_search_results_form($form, $form_state) {
  $form['live_results'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live search results settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['live_results']['apachesolr_live_results_show'] = array(
    '#type' => 'radios',
    '#title' => t('Show:'),
    '#description' => t('Defines what is shown in suggestions.'),
    '#options' => array(
      'suggestion' => t('Common searches'),
      'links' => t('Live results'),
      'both' => t('Common searches and live results'),
    ),
    '#default_value' => apachesolr_live_results_variable_get_show(),
  );

  $form['live_results']['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity of live results'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#size' => 10,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#default_value' => apachesolr_live_results_variable_get_quantity(),
  );

  $form['live_results']['auto_submit_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto submiting search box input'),
    '#default_value' => apachesolr_live_results_auto_submit_search(),
    '#description' => t('Switch off, if you do not want - auto submiting search form, for autocomplete suggestions'),
  );

  $fields_array = apachesolr_live_results_get_fields();

  if (!empty($fields_array)) {
    $form['live_results']['content_types_field_set'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content types fields to show settings'),
      '#weight' => 7,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($fields_array as $content_type => $fields) {
      if (!empty($fields)) {

        $big_content_type = ucfirst($content_type);
        $form['live_results']['content_types_field_set'][$content_type] = array(
          '#type' => 'checkboxes',
          '#multiple' => TRUE,
          '#title' => check_plain($big_content_type),
          '#description' => t('Which fields need to be shown in recommended results.'),
          '#options' => $fields,
          '#default_value' => apachesolr_live_results_variable_get_show_in_live_result($content_type),
        );
      }
    }

    $form['#apache_fields'] = $fields_array;
  }

  $form['content_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 44,
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function live_search_results_form_submit($form, &$form_state) {

  if (isset($form['#apache_fields'])) {
    foreach ($form['#apache_fields'] as $content_type => $fields) {
      if (!empty($fields)) {

        foreach ($fields as $key => $value) {
          $settings[$content_type][$key] = $form_state['values'][$content_type][$key];
        }
        variable_set('apachesolr_live_results_show_in_live_result_' . $content_type, $settings[$content_type]);
      }
    }
  }

  variable_set('apachesolr_live_results_show', $form_state['values']['apachesolr_live_results_show']);
  variable_set('auto_submit_search', $form_state['values']['auto_submit_search']);
  variable_set('apachesolr_live_results_quantity', $form_state['values']['quantity']);
}
